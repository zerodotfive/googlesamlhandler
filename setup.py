from setuptools import setup

__version__ = "0.0.1"

setup(
    name="googlesamlhandler",
    version=__version__,
    zip_safe=False,
    packages=["googlesamlhandler"],
)
